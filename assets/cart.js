/* ********************************** */
/* Custom Scripts */
/* ********************************** */
(function ($) {
    "use strict";

    var cartCount = $('.cart-count');
    var cartTotal = $('.cart-total');
    var cartContainer = $('#cart-sidebar .sidebar-content');
    var cartFooter = $('.cart-footer');
    var cartFooterEmpty = $('.cart-footer-empty');
    var cartDelBtn = '.remove-from-cart';

    function getCart(){

        cartContainer.html('');

        jQuery.getJSON('/cart.js', function(cart) {

            if( cartCount.length && cart.item_count !== 0){
                cartCount.css({
                    "visibility": "visible",
                    "opacity": "1"
                })
                cartCount.each(function(){
                    $(this).text(cart.item_count);
                })
            }else {
                cartCount.css({
                    "visibility": "hidden",
                    "opacity": "0"
                })
            }

            if( cartTotal.length && cart.item_count !== 0){
                cartTotal.each(function(){
                    $(this).text('( '+ Shopify.formatMoney(cart.total_price)+ ' )');
                })
            }

            if( cartContainer.length && cart.item_count !== 0){
                jQuery.each( cart.items, function( i, item ) {
                    cartContainer.append( 
                        `
                        <div class="menu-card d-flex justify-content-between">
                            <div class="d-flex align-items-center w-75">
                                <div class="image flex-img mr-2">
                                    <img src="`+ item.image +`" alt="">
                                </div>
                                <div class="item-info">
                                    <h4 class="fz18 text-truncate fw-600">`+ item.title +`</h4>
                                    <p class="fz14">price: <strong>`+ Shopify.formatMoney(item.price) + ` (`+ item.quantity +`)`+`</strong></p> 
                                </div>
                            </div>
                            <div class="item-action d-flex align-items-center">
                                <button class="remove-from-cart btn btn-sm btn-danger mdi mdi-close fz18" data-id="`+item.id+`"></button>
                            </div>
                        </div>
                        `
                    );
                });

                if(cartFooter.hasClass('d-none')){
                    cartFooter.removeClass('d-none');
                }
                if(!cartFooterEmpty.hasClass('d-none')){
                    cartFooterEmpty.addClass('d-none');
                }

            }else {
                cartFooter.addClass('d-none');
                cartFooterEmpty.removeClass('d-none');
            }

        });
    }

    function addCart() {
        if($('.ajax-cart-form').length){
            $('.ajax-cart-form').on('submit',function(e){
                var form = $(this);
                e.preventDefault();
                var btn = $(this).find('button[type="submit"]');
                btn.prop("disabled",true);
                btn.css({
                    "opacity": ".7"
                })
                jQuery.ajax({
                    type: 'POST',
                    url: '/cart/add.js',
                    data: form.serialize(),
                    dataType: 'json'
                }).done(function() {
                    btn.prop("disabled",false);
                    btn.css({
                        "opacity": "1"
                    })

                    getCart();

                    alertify.success('Added To Cart')
                    
                }).fail(function(res) {
                    if(res.status == 422){
                        alertify.error(res.responseJSON.description)
                    }
                })

            })
        }   
    }

    function removeCartItem (){
        $(document).on("click", cartDelBtn, function(e){
            e.preventDefault();

            var productId = $(this).attr('data-id');

            jQuery.ajax({
                type: 'POST',
                url: '/cart/change.js',
                data: {
                    quantity: 0,
                    id: productId
                },
                dataType: 'json'
            }).done(function() {

                getCart();
                alertify.success('Item removed')

            }).fail(function(res) {
                if(res.status == 422){
                    alert(res.responseJSON.description);
                }
            })

        })
    }


    /*==========================================================================
        WHEN DOCUMENT LOADING
    ==========================================================================*/
    $(window).on('load', function () {
        getCart();
        addCart();
        removeCartItem();
    });

})(window.jQuery);