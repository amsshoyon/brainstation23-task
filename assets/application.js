/* ********************************** */
/* Custom Scripts */
/* ********************************** */
(function ($) {
    "use strict";

    /*==========================================================================
        :: All Essential Functions
    ==========================================================================*/
    function inputAnimate() {
        if ($('.form-control').length) {

            // Check if has value
            $('.form-control').each(function () {
                if ($(this).val()) {
                    $(this).parents(".form-group").addClass('focus');
                }
            })

            // Events
            $('.form-control').focus(function () {
                $(this).parents(".form-group").addClass('focus');
            });

            $(".form-control").focusout(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    $(this).parents(".form-group").removeClass('focus');
                };
            });

            $(".form-control").on('change keyup paste', function (e) {
                // alert($(this).val());
                if ($(this).val() == '' || $(this).val() == null) {
                    $(this).parents(".form-group").addClass('error');
                } else {
                    $(this).parents(".form-group").removeClass('error');
                    $(this).parents(".form-group").addClass('focus');
                }
            });
        }
    }

    function menuHide() {
        $('main').removeClass('overlay');
        $('.menu-toggler').removeClass('show');
        setTimeout(function () {
            $('.menu-toggler').removeClass('animate');
        }, 300);
        $('.site-header .menu-area').removeClass('show');
        $('body').css({
            'overflow-y': 'visible'
        })
    }

    function menuShow() {
        $('main').addClass('overlay');
        $('.menu-toggler').addClass('animate');
        setTimeout(function () {
            $('.menu-toggler').addClass('show');
        }, 400);
        $('.site-header .menu-area').addClass('show');
        $('body').css({
            'overflow-y': 'hidden'
        })
    }

    function menuToggler() {
        var btn = $('.menu-toggler');
        if (btn.length) {
            btn.on('click', function () {
                if (btn.hasClass('animate')) {
                    menuHide();
                } else {
                    menuShow();
                }

            })

            $(document).click(function(event) {
                if(
                    !$(event.target).is($(this).find('.menu-toggler')) &&
                    !$(event.target).is($(this).find('.menu-toggler *')) &&
                    !$(event.target).is($(this).find('.menu-area')) &&
                    !$(event.target).is($(this).find('.menu-area *'))
                ){
                    menuHide();
                }
            });
        }
    }

    function pageLoader() {
        if ($('.preloader').length) {
            $('.preloader').addClass('visible');
        }
    }

    function goTop() {
        $('#go-top').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 900);
        });
    }

    function dropdown() {
        if($('[data-toggle="dropdown"]').length){
            var btn = $('[data-toggle="dropdown"]');
            btn.on('click', function(e){
                e.preventDefault();
                $(this).siblings('.dropdown-menu').toggleClass('show');
            })

            $(document).click(function(e) {
              $('.dropdown').not($('.dropdown').has($(e.target))).children('.dropdown-menu').removeClass('show');
            });
        }
    }

    function sidebar() {
        var menu = $('[data-toggle="sidebar"]');

        menu.on('click', function(e){
            e.preventDefault();
            var sidebar = $("#" + $(this).attr('data-ref'));
            sidebar.addClass('show');
            $("body").css({"overflow-y": "hidden"});
        })

        $('.sidebar').click(function(event) {
            if(
                !$(event.target).is($(this).find('.sidebar-container')) &&
                !$(event.target).is($(this).find('.sidebar-container *'))
            ){
                $(this).removeClass("show");
            }
        })

        $('.sidebar-close-btn').on('click', function(){
            $(this).parents('.sidebar').removeClass('show')
        })
    }

    function productSlider() {
        if($('.product-main-images').length){
            $('.product-main-images').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.product-thumbs').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.product-main-images',
                dots: false,
                arrows: false,
                centerMode: true,
                centerPadding: '45px'
            });
        }
    }


    /*==========================================================================
        WHEN DOCUMENT LOADING
    ==========================================================================*/
    $(window).on('load', function () {

        alertify.defaults = {
            transition:'fade',
            notifier:{
                delay: 3,
                position:'top-left',
                classes : {
                    base: 'alertify-notifier border',
                    visible: 'ajs-visible',
                }
            }
        };

        menuToggler();
        sidebar();
        inputAnimate();
        goTop();
        dropdown();
        productSlider();
        // will load on end
        pageLoader();

    });

    /*==========================================================================
        WHEN WINDOW SCROLL
    ==========================================================================*/
    $(window).scroll(function () {
        if ($(window).scrollTop() < 500) {
            $('.back-to-top').removeClass('show');
        } else {
            $('.back-to-top').addClass('show');
        }
    });

    /*==========================================================================
        WHEN WINDOW RESIZE
    ==========================================================================*/
    $(window).on("resize", function () {
        //
    });

})(window.jQuery);




