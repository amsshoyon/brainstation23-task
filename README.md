## Test task

```
Shop Url: https://rsshophy.myshopify.com   (password: 'password')
```
```
Page Url: https://rsshophy.myshopify.com/pages/test

```
File for the task: templates/page.test.liquid

```
// Bootstrap 4 carousel Applied for the slider
// ----------------------------------------------
<style>
	.flex-img-contain img{
		object-fit: contain;
	}
	.carousel-control-next,
	.carousel-control-prev {
		width: 48px !important;
		height: 48px;
		background: rgba(0, 0, 0, .5);
		top: -165px;
	}
	.carousel-control-prev {
		right: 0px;
		left: auto;
	}
	.carousel-control-next {
		right: -54px;
	}
</style>

<section class="shop-by section-pt section-pb">
	<div class="container">
		<div class="title mb-5 pb-5">
			<h3 class="fz40 lh-1 fw-700 text-uppercase mb-2">Bootstrap 4 Carousel</h3>
		</div>
		<div id="demo" class="carousel slide" data-ride="carousel">

			<div class="carousel-inner">
				{% for product in collections['apple'].products limit:5 %}
				<div class="item carousel-item {% if forloop.first == true %} active {% endif %}">
				<div class="row">
					<div class="col-md-5">
						<div class="image flex-img flex-img-contain mb-3">
							<img src="{{ product.featured_image.src | img_url: 'large' }}" alt="">
						</div>
					</div>
					<div class="col-md-7">
						<h3 class="mb-4 fz28 fw-600 text-uppercase">{{  product.title }}</h3>
						<p class="fz16">{{product.description}}</p>
						<div class="button-group mt-5">
							<a href="{{ product.url }}" class="btn btn-primary fz24 px-4">SHOP NOW</a>
						</div>
					</div>
				</div>
				</div>
				{% endfor %}
			</div>

			<!-- Left and right controls -->
			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			</a>
			<a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			</a>

		</div>
	</div>
</section>

```